

const menu = document.querySelector('.header-nav--mobile');
const menuToggle = document.querySelector('[data-menu-toggle]');
const menuClose = document.querySelector('[data-menu-close]');

menuToggle.addEventListener('click', () => {
  menu.classList.toggle('active');
})

menuClose.addEventListener('click', () => {
  menu.classList.remove('active');
})

var acc = document.getElementsByClassName("accordion__button");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    console.log('click')
    /* Toggle between adding and removing the "active" class,
    to highlight the button that controls the panel */
    this.classList.toggle("active");

    /* Toggle between hiding and showing the active panel */
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}


//javascript for mobile menu icons
// const svg = document.querySelector(".mobile__menu__arrow");

const subToggle = document.querySelectorAll('[data-submenu-toggle]');
const subMenu = document.querySelectorAll('[data-submenu]');



subToggle.forEach((toggle) => {
    toggle.addEventListener('click', () => {
        // subArrow.classList.toggle("active")
        subMenu.forEach((menu) => {
            if (menu.dataset.submenu == toggle.dataset.submenuToggle) {
                menu.classList.toggle("active")
                toggle.classList.toggle("active")

            }
        })
    })
})







/**
* Try this example at https://alpsquid.github.io/quizlib
*/

/** Key value pairs using quiz element IDs and Quiz Objects.
 * For example: quizzes['quiz-1'] = [Quiz Object]
 */
var quizzes = {};

/**
 * Callback for answer buttons. The implementation for this will vary depending on your requirements.
 * In this example, the same function is being used for every quiz so we pass the ID of the quiz element and
 * retrieve the respective quiz instance from the quiz map we created in the window.onload function.
 */
function showResults(quizID) {

    // Retrieve the quiz instance for this quiz element from the map.
    var activeQuiz = quizzes[quizID];
    // Check answers and continue if all questions have been answered
    if (activeQuiz.checkAnswers()) {
        var quizScorePercent = activeQuiz.result.scorePercentFormatted; // The unformatted percentage is a decimal in range 0 - 1
        var quizResultElement = document.getElementById('quiz-result');

        //show the explanation
        const answerExplanations = document.querySelectorAll(".quizlib-explanation");
        console.log(answerExplanations);
        answerExplanations.forEach((explanation) => {
            explanation.classList.add("active")
        })


        // Move the quiz result element to the active quiz, placing it after the quiz title.
        var quizElement = document.getElementById(quizID);
        quizElement.insertBefore(quizResultElement, quizElement.children[0]);

        // Show the result element and add result values.
        quizResultElement.style.display = 'block';
        document.getElementById('quiz-score').innerHTML = activeQuiz.result.score.toString();
        document.getElementById('quiz-max-score').innerHTML = activeQuiz.result.totalQuestions.toString();
        document.getElementById('quiz-percent').innerHTML = quizScorePercent.toString();

        // Change background colour of results div according to score percent
        if (quizScorePercent >= 75) quizResultElement.style.backgroundColor = '#4caf50';
        else if (quizScorePercent >= 50) quizResultElement.style.backgroundColor = '#ffc107';
        else if (quizScorePercent >= 25) quizResultElement.style.backgroundColor = '#ff9800';
        else if (quizScorePercent >= 0) quizResultElement.style.backgroundColor = '#f44336';
        
        // Highlight questions according to whether they were correctly answered. The callback allows us to highlight/show the correct answer
        activeQuiz.highlightResults(handleAnswers);
    }
}

/** Callback for Quiz.highlightResults. Highlights the correct answers of incorrectly answered questions 
 * Parameters are: the quiz object, the question element, question number, correctly answered flag
 */
function handleAnswers(quiz, question, no, correct) {
    if (!correct) {
        var answers = question.getElementsByTagName('input');
        for (var i = 0; i < answers.length; i++) {
            if (answers[i].type === "checkbox" || answers[i].type === "radio"){ 
                // If the current input element is part of the correct answer, highlight it
                if (quiz.answers[no].indexOf(answers[i].value) > -1) {
                    answers[i].parentNode.classList.add(Quiz.Classes.CORRECT);

                }
            } else {
                // If the input is anything other than a checkbox or radio button, show the correct answer next to the element
                var correctAnswer = document.createElement('span');
                correctAnswer.classList.add(Quiz.Classes.CORRECT);
                correctAnswer.classList.add(Quiz.Classes.TEMP); // quiz.checkAnswers will automatically remove elements with the temp class
                correctAnswer.innerHTML = quiz.answers[no];
                correctAnswer.style.marginLeft = '10px';
                answers[i].parentNode.insertBefore(correctAnswer, answers[i].nextSibling);
            }
        }
    }

}

window.onload = function() {
    // Create quiz instances for each quiz and add them to the quizzes map.
    // The key is the ID of the quiz element, same as what we pass to the Quiz object as the first argument.
    //need to check what page somehow
    const quiz1 = document.querySelector("#quiz-1");
    const quiz2 = document.querySelector("#quiz-2");
    const quiz3 = document.querySelector("#quiz-3");
    const quiz4 = document.querySelector("#quiz-4");
    const quiz5 = document.querySelector("#quiz-5");

    if (quiz1) {
       quizzes['quiz-1'] = new Quiz('quiz-1', [
            'b',
            'd',
            'b'
        ]);
    };

    if (quiz2) {
        quizzes['quiz-2'] = new Quiz('quiz-2', [
            'd',
            'd',
            'b',
            'd',
            'c',
            'c'
        ]);
    };

    if (quiz3) {
        quizzes['quiz-3'] = new Quiz('quiz-3', [
            'd',
            'a',
            'd',
            'a',
            'd',
            'c',
            'd',
            'c',
            'd'
        ]);
    };

    if (quiz4) {
        quizzes['quiz-4'] = new Quiz('quiz-4', [
            'b',
            'b',
            'b'
        ]);
    };

    if (quiz5) {
        quizzes['quiz-5'] = new Quiz('quiz-5', [
            'd',
            'a',
            'a',
            'e'
        ])
    };
};





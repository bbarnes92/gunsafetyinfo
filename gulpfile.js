var gulp = require('gulp');
var sass = require('gulp-sass');
var terser = require('gulp-terser');

gulp.task('css', function(){
   return gulp.src('assets/css/main.scss')
      .pipe(sass())
        .on('error', function (err) {
          console.log(err.toString());
          this.emit('end');
          })
      .pipe(gulp.dest('web/css'));
});

gulp.task('js', function(){
    return gulp.src(['assets/js/main.js'])
        .pipe(terser())
        .pipe(gulp.dest('web/js'));
});

gulp.task('vendor-js', function(){
    return gulp.src(['assets/js/vendor/*.js'])
        .pipe(terser())
        .pipe(gulp.dest('web/js/vendor'));
});

gulp.task('vendor-css', function(){
    return gulp.src(['assets/css/vendor/*.css'])
        .pipe(gulp.dest('web/css/vendor'));
});

/* WATCH */
gulp.task('watch', function(){
    gulp.watch('assets/css/**/*.scss', gulp.series('css'));
    gulp.watch('assets/js/*.js', gulp.series('js'));
    gulp.watch('assets/js/vendor/*.js', gulp.series('vendor-js'));
    gulp.watch('assets/css/vendor/*.css', gulp.series('vendor-css'));
});